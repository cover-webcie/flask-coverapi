'''
Flask-CoverAPI
--------------

Flask-CoverAPI provides CoverAPI support for Flask. It provides user session 
management for svcover.nl-global sessions in a similar way to Flask-Login. In 
addition to that, it provides some decorators for basic permissions based on 
member- and committee status.
'''

from setuptools import setup, find_packages


setup(
    name='Flask-CoverAPI',
    version='1.0',
    author='Martijn Luinstra',
    author_email='martijnluinstra@gmail.com',
    description='Flask extension for CoverAPI',
    long_description=__doc__,
    packages=find_packages(),
    zip_safe=False,
    platforms='any',
    install_requires=[
        'CoverAPI@git+https://bitbucket.org/cover-webcie/coverapi.git@v0.1',
        'Flask>=0.8.0'
    ],
)
