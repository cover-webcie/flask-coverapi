from functools import wraps
from urllib.parse import urlparse, urlunparse

from flask import current_app, redirect, request, abort, has_request_context

from werkzeug.local import LocalProxy
from werkzeug.urls import url_decode, url_encode

from .config import COVER_LOGIN_URL, COVER_LOGOUT_URL


current_user = LocalProxy(lambda: _get_user())


def _cover_session_url(base, next_url=None, next_field='referrer'):
    if next_url is None:
        if has_request_context():
            next_url = request.url
        else:
            return base

    parts = list(urlparse(base))
    md = url_decode(parts[4])
    md[next_field] = next_url
    parts[4] = url_encode(md, sort=True)
    return urlunparse(parts)


def login_url(**kwargs):
    base = current_app.config.get('COVER_LOGIN_URL', COVER_LOGIN_URL)
    return _cover_session_url(base, **kwargs)


def logout_url(**kwargs):
    base = current_app.config.get('COVER_LOGOUT_URL', COVER_LOGOUT_URL)
    return _cover_session_url(base, **kwargs)


def login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(login_url())
        return func(*args, **kwargs)
    return decorated_view


def member_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(login_url())
        if not current_user.is_member:
            abort(403)
        return func(*args, **kwargs)
    return decorated_view


def admin_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(login_url())
        if not current_user.is_admin:
            abort(403)
        return func(*args, **kwargs)
    return decorated_view


def committee_required(**committees):
    def impl(func):
        @wraps(func)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated:
                return redirect(login_url())
            if not current_user.in_committee(committees):
                abort(403)
            return func(*args, **kwargs)
        return decorated_view
    return impl


def _get_user():
    if has_request_context():
        return current_app.cover_session_manager.get_session()
    return None


def _user_context_processor():
    return dict(current_cover_user=_get_user(), login_url=login_url, logout_url=logout_url)
