COVER_API_URL = 'https://www.svcover.nl/api'
COVER_LOGIN_URL = 'https://www.svcover.nl/login'
COVER_LOGOUT_URL = 'https://www.svcover.nl/logout'
COVER_COOKIE = 'cover_session_id'
COVER_ADMINS = []

MEMBER_STATUS_LID = 1
