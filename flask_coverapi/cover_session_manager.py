from flask import current_app, request, g

from coverapi import CoverAPI, APIError, CoverSession as _CoverSession

from .config import COVER_ADMINS, COVER_API_URL, COVER_COOKIE, MEMBER_STATUS_LID
from .utils import _user_context_processor


class CoverSessionManager:
    def __init__(self, **kwargs):
        self.anonymous_class = AnonymousSession
        self.session_class = CoverSession
        self._api = None

        flask_app = kwargs.pop('flask_app', None)
        add_context_processor = kwargs.pop('add_context_processor', True)
        
        self._api_kwargs = kwargs

        if flask_app:
            self.init_app(flask_app, add_context_processor)

    def init_app(self, app, add_context_processor=True):
        app.cover_session_manager = self

        self._api = self.init_api(app)

        if add_context_processor:
            app.context_processor(_user_context_processor)

    def init_api(self, flask_app):
        kwargs = self._api_kwargs
        url = kwargs.pop('url', flask_app.config.get('COVER_API_URL', COVER_API_URL))
        app = kwargs.pop('app', flask_app.config['COVER_APP'])
        secret = kwargs.pop('app', flask_app.config['COVER_SECRET'])
        return CoverAPI(url, app, secret, **kwargs)

    @property
    def api(self):
        if not self._api:
            self._api = self.init_api(current_app)
        return self._api

    def get_session(self, session_id=None):
        if not session_id:
            cover_cookie = current_app.config.get('COVER_COOKIE', COVER_COOKIE)
            session_id = request.cookies.get(cover_cookie, 0)

        session = g.get("_cover_session")
        if session and not session.is_anonymous and session.session_id == session_id:
            return session
        
        try:
            response = self.request_json({
                'method': 'session_get_member',
                'session_id': session_id
            })
        except APIError as e:
            g._cover_session = self.anonymous_class()
        else:
            g._cover_session = self.session_class(self, session_id, response['result'])
        return g._cover_session

    def request(self, *args, **kwargs):
        return self.api.request(*args, **kwargs)

    def request_json(self, *args, **kwargs):
        return self.api.request_json(*args, **kwargs)

    def login(self, *args, **kwargs):
        return self.api.login(*args, **kwargs)


class CoverSession(_CoverSession):
    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        return True
  
    @property
    def is_member(self):
        return self.user['type'] == MEMBER_STATUS_LID

    @property
    def is_admin(self):
        admins = current_app.config.get('COVER_ADMINS', COVER_ADMINS)
        if not admins:
            return False
        if self.get_id() in admins:
            return True
        return self.in_committee(*admins)

    @property
    def full_name(self):
        if self.user["tussenvoegsel"]:
            return ' '.join([self.user["voornaam"], self.user["tussenvoegsel"], self.user["achternaam"]])
        return ' '.join([self.user["voornaam"], self.user["achternaam"]])

    @property
    def committees(self):
        try:
            return self.user['committees'].keys()
        except AttributeError:
            if isinstance(self.user['committees'], list):
                return self.user['committees']
            return []

    @property
    def id(self):
        return self.get_id()

    def get_id(self):
        return self.user['id']

    def in_committee(self, *args):
        return any(committee in self.committees for committee in args)


class AnonymousSession(object):
    @property
    def is_anonymous(self):
        return True

    @property
    def is_authenticated(self):
        return False
  
    @property
    def is_member(self):
        return False

    @property
    def is_admin(self):
        return False

    @property
    def committees(self):
        return []

    @property
    def id(self):
        return self.get_id()
    
    def get_id(self):
        return None

    def in_committee(self, *args):
        return False
