# Flask-CoverAPI

Flask-CoverAPI provides CoverAPI support for Flask. It provides user session 
management for svcover.nl-global sessions in a similar way to Flask-Login. In 
addition to that, it provides some decorators for basic permissions based on 
member- and committee status.
